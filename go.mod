module git.happydns.org/happydns

go 1.14

require (
	github.com/go-mail/mail v2.3.1+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/miekg/dns v1.1.29
	github.com/ovh/go-ovh v0.0.0-20181109152953-ba5adb4cf014
	github.com/oze4/godaddygo v1.3.18-0.20210106032252-2dd767845da9
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/syndtr/goleveldb v1.0.0
	github.com/yuin/goldmark v1.2.1
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/ini.v1 v1.55.0 // indirect
	gopkg.in/mail.v2 v2.3.1 // indirect
)
