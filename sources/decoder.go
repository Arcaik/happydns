// Copyright or © or Copr. happyDNS (2020)
//
// contact@happydns.org
//
// This software is a computer program whose purpose is to provide a modern
// interface to interact with DNS systems.
//
// This software is governed by the CeCILL license under French law and abiding
// by the rules of distribution of free software.  You can use, modify and/or
// redistribute the software under the terms of the CeCILL license as
// circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".
//
// As a counterpart to the access to the source code and rights to copy, modify
// and redistribute granted by the license, users are provided only with a
// limited warranty and the software's author, the holder of the economic
// rights, and the successive licensors have only limited liability.
//
// In this respect, the user's attention is drawn to the risks associated with
// loading, using, modifying and/or developing or reproducing the software by
// the user in light of its specific status of free software, that may mean
// that it is complicated to manipulate, and that also therefore means that it
// is reserved for developers and experienced professionals having in-depth
// computer knowledge. Users are therefore encouraged to load and test the
// software's suitability as regards their requirements in conditions enabling
// the security of their systems and/or data to be ensured and, more generally,
// to use and operate it in the same conditions as regards security.
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package sources // import "happydns.org/sources"

import (
	"fmt"
	"log"
	"reflect"

	"git.happydns.org/happydns/model"
)

// SourceCreator abstract the instanciation of a Source
type SourceCreator func() happydns.Source

// Source aggregates way of create a Source and information about it.
type Source struct {
	Creator SourceCreator
	Infos   SourceInfos
}

// sources stores all existing Source in happyDNS.
var sources map[string]Source = map[string]Source{}

// RegisterSource declares the existence of the given Source.
func RegisterSource(creator SourceCreator, infos SourceInfos) {
	baseType := reflect.Indirect(reflect.ValueOf(creator())).Type()
	name := baseType.String()
	log.Println("Registering new source:", name)

	sources[name] = Source{
		creator,
		infos,
	}
}

// GetSources retrieves the list of all existing Sources.
func GetSources() *map[string]Source {
	return &sources
}

// FindSource returns the Source corresponding to the given name, or an error if it doesn't exist.
func FindSource(name string) (happydns.Source, error) {
	src, ok := sources[name]
	if !ok {
		return nil, fmt.Errorf("Unable to find corresponding source for `%s`.", name)
	}

	return src.Creator(), nil
}
